
a_partition = {1:[1,1,100,0],2:[2,1,200,0],5:[5,1,500,0]}
a_op_av = {"identifier":"C:","cylinders":500000, "heads":126, "sectors":33}

a_type = {0:"Vacía",24:"DOS de NEC" ,81:"Minix / Linux a bf  Solaris",
 1:"FAT12",27:"WinRE NTFS ocul", 82:"Linux swap / So", "c1":"DRDOS/sec (FAT-",
 2: "XENIX root"      ,39:  "Plan 9 "        , 83:  "Linux"           ,"c4":  "DRDOS/sec (FAT-",
 3:  "XENIX usr  "     ,"3c":  "PartitionMagic"  ,84: " OS/2 hidden or"  ,"c6":  "DRDOS/sec (FAT-",
 4:  "FAT16 <32M"      ,40:  "Venix 80286  "   ,85: " Linux extendida" ,"c7":  "Syrinx",
 5:  "Extendida  "     ,41:  "PPC PReP Boot "  ,86: " Conjunto de vol" ,"da":  "Datos sin SF",
 6:  "FAT16   "        ,42:  "SFS  "           ,87: " Conjunto de vol ","db":  "CP/M / CTOS / .",
 7:  "HPFS/NTFS/exFAT" ,"4d":  "QNX4.x  "        ,88: " Linux plaintext ","de":  "Utilidad Dell",
 8:  "AIX   "          ,"4e": "QNX4.x segunda  ","8e": " Linux LVM  "     ,"df":  "BootIt"
}

wellcome_txt = """\nWelcome to fdisk (util-linux 2.27.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command."""

help_txt="\nOrden (m para obtener ayuda):"

help_av_txt="\nOrden avanzada(m para obtener ayuda):"


menu_txt = """Help:

  DOS (MBR)
   a   toggle a bootable flag
   b   edit nested BSD disklabel
   c   toggle the dos compatibility flag

  Generic
   d   delete a partition
   F   list free unpartitioned space
   l   list known partition types
   n   add a new partition
   p   print the partition table
   t   change a partition type
   v   verify the partition table
   i   print information about a partition

  Misc
   m   print this menu
   u   change display/entry units
   x   extra functionality (experts only)

  Script
   I   load disk layout from sfdisk script file
   O   dump disk layout to sfdisk script file

  Save & Exit
   w   write table to disk and exit
   q   quit without saving changes

  Create a new label
   g   create a new empty GPT partition table
   G   create a new empty SGI (IRIX) partition table
   o   create a new empty DOS partition table
   s   create a new empty Sun partition table
"""

menu_av_txt = """Help (expert commands):

  DOS (MBR)
   b   move beginning of data in a partition
   i   change the disk identifier

  Geometría
   c   change number of cylinders
   h   change number of heads
   s   change number of sectors/track

  Generic
   p   print the partition table
   v   verify the partition table
   d   print the raw data of the first sector from the device
   D   print the raw data of the disklabel from the device
   f   fix partitions order
   m   print this menu

  Save & Exit
   q   quit without saving changes
   r   return to main menu
"""

f_txt = """Unpartitioned space /dev/sda: 3 MiB, 3112448 bytes, 6079 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes

   Start    Final Sectores   Size
      63     2047     1985 992,5K
16777216 16779261     2046  1023K
20969472 20971519     2048     1M
"""

l_txt_BKP="""\n 0  Vacía           24  DOS de NEC      81  Minix / Linux a bf  Solaris
 1  FAT12           27  WinRE NTFS ocul 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extendida c7  Syrinx
 5  Extendida       41  PPC PReP Boot   86  Conjunto de vol da  Datos sin SF
 6  FAT16           42  SFS             87  Conjunto de vol db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Utilidad Dell
 8  AIX             4e  QNX4.x segunda  8e  Linux LVM       df  BootIt
 9  AIX arrancable  4f  QNX4.x tercera  93  Amoeba          e1  DOS access
 a  Gestor de arran 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  Hibernación de  ea  Rufus alignment
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ee  GPT
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  FAT12 oculta    56  Golden Bow      a8  UFS de Darwin   f0  inicio Linux/PA
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f1  SpeedStor
14  FAT16 oculta <3 61  SpeedStor       ab  arranque de Dar f4  SpeedStor
16  FAT16 oculta    63  GNU HURD o SysV af  HFS / HFS+      f2  DOS secondary
17  HPFS/NTFS ocult 64  Novell Netware  b7  BSDI fs         fb  VMFS de VMware
18  SmartSleep de A 65  Novell Netware  b8  BSDI swap       fc  VMKCORE de VMwa
1b  FAT32 de W95 oc 70  DiskSecure Mult bb  Boot Wizard hid fd  Linux raid auto
1c  FAT32 de W95 (L 75  PC/IX           bc  Acronis FAT32 L fe  LANstep
1e  FAT16 de W95 (L 80  Minix antiguo   be  arranque de Sol ff  BBT
"""

l_txt="""\n 0  Vacía           24  DOS de NEC      81  Minix / Linux a bf  Solaris
 1  FAT12           27  WinRE NTFS ocul 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extendida c7  Syrinx
 5  Extendida       41  PPC PReP Boot   86  Conjunto de vol da  Datos sin SF
 6  FAT16           42  SFS             87  Conjunto de vol db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Utilidad Dell
 8  AIX             4e  QNX4.x segunda  8e  Linux LVM       df  BootIt
"""

p_txt = """Disk /dev/sda: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x4f52bb73

Disposit.  Inicio    Start    Final Sectores Size Id Tipo
/dev/sda1  *          2048 16777215 16775168   8G 83 Linux
/dev/sda2         16779262 20969471  4190210   2G  5 Extendida
/dev/sda5         16779264 20969471  4190208   2G 82 Linux swap / Solaris
"""

w_txt = """ The partition table has been altered.
Calling ioctl() to re-read partition table.
Re-reading the partition table failed.: Dispositivo o recurso ocupado

The kernel still uses the old table. The new table will be used at the next reboot or after you run partprobe(8) or kpartx(8).
"""

info_partition_txt="""
         Device: /dev/sda2
          Start: 16779262
            End: 20969471
        Sectors: 4190210
      Cylinders: 261
           Size: 2G
             Id: 5
           Type: Extendida
    Start-C/H/S: 1023/63/254
      End-C/H/S: 1023/63/254
"""

p_av_txt="""\nDisk /dev/sda: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x4f52bb73

Disposit.  Inicio    Start    Final Sectores Id Tipo           Start-C/H/S   End-C/H/S Attrs
/dev/sda1  *          2048 16777215 16775168 83 Linux              0/33/32 1023/63/254    80
/dev/sda2         16779262 20969471  4190210  5 Extendida      1023/63/254 1023/63/254
/dev/sda5         16779264 20969471  4190208 82 Linux swap / S 1023/63/254 1023/63/254
"""

d_av_txt="""\nFirst sector: offset = 0, size = 512 bytes.
00000000  eb 63 90 10 8e d0 bc 00  b0 b8 00 00 8e d8 8e c0
00000010  fb be 00 7c bf 00 06 b9  00 02 f3 a4 ea 21 06 00
00000020  00 be be 07 38 04 75 0b  83 c6 10 81 fe fe 07 75
00000030  f3 eb 16 b4 02 b0 01 bb  00 7c b2 80 8a 74 01 8b
00000040  4c 02 cd 13 ea 00 7c 00  00 eb fe 00 00 00 00 00
00000050  00 00 00 00 00 00 00 00  00 00 00 80 01 00 00 00
00000060  00 00 00 00 ff fa 90 90  f6 c2 80 74 05 f6 c2 70
00000070  74 02 b2 80 ea 79 7c 00  00 31 c0 8e d8 8e d0 bc
00000080  00 20 fb a0 64 7c 3c ff  74 02 88 c2 52 bb 17 04
00000090  f6 07 03 74 06 be 88 7d  e8 17 01 be 05 7c b4 41
000000a0  bb aa 55 cd 13 5a 52 72  3d 81 fb 55 aa 75 37 83
000000b0  e1 01 74 32 31 c0 89 44  04 40 88 44 ff 89 44 02
000000c0  c7 04 10 00 66 8b 1e 5c  7c 66 89 5c 08 66 8b 1e
000000d0  60 7c 66 89 5c 0c c7 44  06 00 70 b4 42 cd 13 72
000000e0  05 bb 00 70 eb 76 b4 08  cd 13 73 0d 5a 84 d2 0f
000000f0  83 d0 00 be 93 7d e9 82  00 66 0f b6 c6 88 64 ff
00000100  40 66 89 44 04 0f b6 d1  c1 e2 02 88 e8 88 f4 40
00000110  89 44 08 0f b6 c2 c0 e8  02 66 89 04 66 a1 60 7c
00000120  66 09 c0 75 4e 66 a1 5c  7c 66 31 d2 66 f7 34 88
00000130  d1 31 d2 66 f7 74 04 3b  44 08 7d 37 fe c1 88 c5
00000140  30 c0 c1 e8 02 08 c1 88  d0 5a 88 c6 bb 00 70 8e
00000150  c3 31 db b8 01 02 cd 13  72 1e 8c c3 60 1e b9 00
00000160  01 8e db 31 f6 bf 00 80  8e c6 fc f3 a5 1f 61 ff
00000170  26 5a 7c be 8e 7d eb 03  be 9d 7d e8 34 00 be a2
00000180  7d e8 2e 00 cd 18 eb fe  47 52 55 42 20 00 47 65
00000190  6f 6d 00 48 61 72 64 20  44 69 73 6b 00 52 65 61
000001a0  64 00 20 45 72 72 6f 72  0d 0a 00 bb 01 00 b4 0e
000001b0  cd 10 ac 3c 00 75 f4 c3  73 bb 52 4f 00 00 80 20
000001c0  21 00 83 fe ff ff 00 08  00 00 00 f8 ff 00 00 fe
000001d0  ff ff 05 fe ff ff fe 07  00 01 02 f0 3f 00 00 00
000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa
"""
D_av_txt="""\nMBR: offset = 0, size = 512 bytes.
00000000  eb 63 90 10 8e d0 bc 00  b0 b8 00 00 8e d8 8e c0
00000010  fb be 00 7c bf 00 06 b9  00 02 f3 a4 ea 21 06 00
00000020  00 be be 07 38 04 75 0b  83 c6 10 81 fe fe 07 75
00000030  f3 eb 16 b4 02 b0 01 bb  00 7c b2 80 8a 74 01 8b
00000040  4c 02 cd 13 ea 00 7c 00  00 eb fe 00 00 00 00 00
00000050  00 00 00 00 00 00 00 00  00 00 00 80 01 00 00 00
00000060  00 00 00 00 ff fa 90 90  f6 c2 80 74 05 f6 c2 70
00000070  74 02 b2 80 ea 79 7c 00  00 31 c0 8e d8 8e d0 bc
00000080  00 20 fb a0 64 7c 3c ff  74 02 88 c2 52 bb 17 04
00000090  f6 07 03 74 06 be 88 7d  e8 17 01 be 05 7c b4 41
000000a0  bb aa 55 cd 13 5a 52 72  3d 81 fb 55 aa 75 37 83
000000b0  e1 01 74 32 31 c0 89 44  04 40 88 44 ff 89 44 02
000000c0  c7 04 10 00 66 8b 1e 5c  7c 66 89 5c 08 66 8b 1e
000000d0  60 7c 66 89 5c 0c c7 44  06 00 70 b4 42 cd 13 72
000000e0  05 bb 00 70 eb 76 b4 08  cd 13 73 0d 5a 84 d2 0f
000000f0  83 d0 00 be 93 7d e9 82  00 66 0f b6 c6 88 64 ff
00000100  40 66 89 44 04 0f b6 d1  c1 e2 02 88 e8 88 f4 40
00000110  89 44 08 0f b6 c2 c0 e8  02 66 89 04 66 a1 60 7c
00000120  66 09 c0 75 4e 66 a1 5c  7c 66 31 d2 66 f7 34 88
00000130  d1 31 d2 66 f7 74 04 3b  44 08 7d 37 fe c1 88 c5
00000140  30 c0 c1 e8 02 08 c1 88  d0 5a 88 c6 bb 00 70 8e
00000150  c3 31 db b8 01 02 cd 13  72 1e 8c c3 60 1e b9 00
00000160  01 8e db 31 f6 bf 00 80  8e c6 fc f3 a5 1f 61 ff
00000170  26 5a 7c be 8e 7d eb 03  be 9d 7d e8 34 00 be a2
00000180  7d e8 2e 00 cd 18 eb fe  47 52 55 42 20 00 47 65
00000190  6f 6d 00 48 61 72 64 20  44 69 73 6b 00 52 65 61
000001a0  64 00 20 45 72 72 6f 72  0d 0a 00 bb 01 00 b4 0e
000001b0  cd 10 ac 3c 00 75 f4 c3  73 bb 52 4f 00 00 80 20
000001c0  21 00 83 fe ff ff 00 08  00 00 00 f8 ff 00 00 fe
000001d0  ff ff 05 fe ff ff fe 07  00 01 02 f0 3f 00 00 00
000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa

EBR: offset = 8590982144, size = 512 bytes.
2000ffc00  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
*
2000ffdb0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 fe
2000ffdc0  ff ff 82 fe ff ff 02 00  00 00 00 f0 3f 00 00 00
2000ffdd0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
*
2000ffdf0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa
"""

def main():
    '''Funcion principal'''
    
    print(wellcome_txt)   
    chk = True

    while chk:
        
        op = input(help_txt)
        chk = menuOption(op)

        
def fn_a():
    '''Habilita o deshabilita particiones'''
    
    op = checkPartition(0)
    
    if op:
        if a_partition[op][1]:
            a_partition[op][1] = 0
            print("The bootable flag on partition "+str(op)+" is disabled now.")
        else:
            a_partition[op][1] = 1
            print("The bootable flag on partition "+str(op)+" is enabled now.") 
    else:
         fn_a()


def fn_b():
    
    
    print("""There is no *BSD partition on /dev/sda.\n
The device (null) does not contain BSD disklabel.\n""")
    
    while True:
        
        a = input("Do you want to create a BSD disklabel? [Y]es/[N]o:")
        
        if a[0] == "Y" or a[0] == "y":            
            print("There is no *BSD partition on /dev/sda.\n")
            break
        elif a[0] == "N" or a[0] == "n":
            break

    
def fn_d():
    '''Elimina una particiones'''
    
    op = checkPartition(0)
    
    if op:
        del a_partition[op]
        print("La partición "+str(op)+" ha sido eliminada.")
    else:
         fn_d()


def fn_f():
    
    print("""Unpartitioned space /dev/sda: 0 B, 0 bytes, 0 sectors
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes""")


def fn_n():
    '''Crea una particion'''
    
    #print("""All space for primary partitions is in use.
    #    Adding logical partition 6
    #    No free sectors available.""")

    op = checkPartition("new")
  
    if type(op) == list:
        a_partition[op[0]]= [op[0], 1, 255000]
        sortPartition()
        print("Particion "+str(op[0])+" creada")
    elif op:
        print("Ya existe esa partición")
    else:
         fn_n()
    
def fn_i():    

    op = checkPartition(0)

    if op:        
        print("Particion: "+str(op))
        print(info_partition_txt)        
    else:
        fn_i()


def fn_t():    
    '''Cambiaria el tipo de particion'''

    op = checkPartition(0)
    if op:
        while True:
        
            tp = input("Partition type (type L to list all types)(default "+str(a_partition[op][3])+")")
            try:           
                tp = int(tp)
            except ValueError:
                pass
                
            if tp == "L":
                print(l_txt)
            else:
                if tp in a_type:
                    a_partition[op][3] = a_type[tp]
                    break
                else:
                    if tp == "q":
                        break
                    else:
                        print("No existe ese tipo")

        
def fn_x():
    '''Funcion de ejecucion del menu avanzado'''
    
    while True:
        
        op = input(help_av_txt)
        
        if op == "b":
            fn_b_av()
        elif op == "i":
            fnIdentifier()
        elif op == "c":
            fnGeometry("cylinders")
        elif op == "h":
            fnGeometry("heads")
        elif op == "s":
            fnGeometry("sectors")
        elif op == "p":
            print(p_av_txt)
        elif op == "v":
            print("Remaining 4095 unallocated 512-byte sectors.")
        elif op == "d":
            print(d_av_txt)
        elif op == "D":
            print(D_av_txt)
        elif op == "f":
            print("Nothing to do. Ordering is correct already.")
        elif op == "m":
            print(menu_av_txt)
        elif op == "q":
            print("\nGood Bye\n")
            return False
        elif op == "r":
            return True


def fn_b_av():

    global a_partition
    
    op = checkPartition("init")

    if op:
        while True:
        
            n = input("Nuevo principio de datos (1-20969471, default "+str(a_partition[op][2])+"):")

            if n == None:
                n = a_partiton[1]

            try:
                n = int(n)
                if n >= 1 and n <= 20969471:   #maximo y minimo de particiones
                    a_partition[op][2]= n
                    break
                else:
                    print("Value out of range.")
            except ValueError:
                print("Not Integer.")


    
def fnGeometry(tp):

    global a_op_av
    
    while True:

        if tp == "cylinders":
            n = input("Número de cilindros (1-1048576, default "+str(a_op_av["cylinders"])+"):")
        elif tp == "heads":
            n = input("Número de cabezas (1-256, default "+str(a_op_av["heads"])+"):")
        elif tp == "sectors":
            n = input("Número de sectores (1-63, default "+str(a_op_av["sectors"])+"):")
   
        if n == None:
            if tp == "cylinders":
                n = a_op_av["cylinders"]
            elif tp == "heads":
                n = a_op_av["heads"]
            elif tp == "sectors":
                n = a_op_av["sectors"]
            
        try:           
            n = int(n)

            if tp == "cylinders" and n >= 1 and n <= 1048576:
                a_op_av["cylinders"] = n
                break
            elif tp == "heads" and n >= 1 and n <= 256:
                a_op_av["cylinders"] = n
                break
            elif tp == "sectors" and n >= 1 and n <= 63:
                a_op_av["sectors"] = n
                break
            else:
                print("Value out of range.")
        except ValueError:
            print("Not Integer.")
        

def fnIdentifier():

    global a_op_av

    n = input("Enter the new disk identifier: (default "+str(a_op_av["identifier"])+"):")
   
    if n != None:
        a_op_av["identifier"] = n
    else:
        print("Incorrect value.")
               

def menuOption(op):
    '''Funcion de ejecucion del menu'''
    
    result = True
    
    if op == "a":
        fn_a()
    elif op == "b":
        fn_b()
    elif op == "c":
        print("DOS Compatibility flag is set (DEPRECATED!)")
    elif op == "d":
        fn_d()
    elif op == "F":
        print(f_txt)
    elif op == "l":
        print(l_txt)
    elif op == "n":
        fn_n()
    elif op == "p":
        print(p_txt)
    elif op == "t":
        fn_t()
    elif op == "v":
        print("Remaining 4095 unallocated 512-byte sectors.")
    elif op == "i":
        fn_i()
    elif op == "m":
        print(menu_txt)
    elif op == "u":
        print("Changing display/entry units to cylinders (DEPRECATED!).")
    elif op == "x":
        if not fn_x():            
            result = False
    elif op == "I":
        readScriptFile(0)
    elif op == "O":
        readScriptFile(1)
    elif op == "w":
        print(w_txt)
        result = False
    elif op == "q":
        result = False
        print("\nGood Bye\n")
    elif op == "g":
        print("Created a new GPT disklabel (GUID: 7C2F767E-C5B9-4C17-8352-965310AD66C4).")
    elif op == "G":
        print("Created a new GPT disklabel (SGI: 7C2F767E-C5B9-4C17-8352-965310AD66C4).")
    elif op == "o":
        print("Created a new GPT disklabel (DOS: 7C2F767E-C5B9-4C17-8352-965310AD66C4).")
    elif op == "s":
        print("Created a new GPT disklabel (SUN: 7C2F767E-C5B9-4C17-8352-965310AD66C4).")
    

    return result


def checkPartition(fn):    
    '''Comprueba las particiones'''
    
    n = input(read_partition())
    if n == None:
        n=5     #Particion por defecto

    try:
        n = int(n)
        if n >= 1 and n <= 6:   #maximo y minimo de particiones
            for x in a_partition:
                if x == n:
                    return x
                    break
            else:
                if fn == "new":
                    return [n]
                elif fn == "init":
                    print("Partition "+str(n)+": no data area.")
                    return False
                else:
                    print("Dont exist this partition")
        else:
            print("Value out of range.")
            return False 
    except ValueError:
        print("Not Integer.")
        return False

    
def read_partition():
    '''Lee las particiones'''
    
    txt = "Número de partición ("
    for x in a_partition:
        txt += str(x)
        txt += ","
        
    txt += " default 5):"
    return txt


def readScriptFile(fn):
    '''fn => segun sea la opcion I u O'''
    
    pth_file = input("Enter script file name:");
    #Faltan comprobaciones de que se hace
    print(pth_file)


def sortPartition():
    '''Ordena la lista de particions'''

    import operator    
    global a_partition

    a_temp = sorted(a_partition.items(), key=operator.itemgetter(0))
    a_new = {}
    for x in a_temp:
        a_new[x[0]] = a_temp[1] 

    a_partition = a_new



    
main() #inicia la ejecucion
